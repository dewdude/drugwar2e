```
  _____  _____  _    _  _______          __     _____       ____  
 |  __ \|  __ \| |  | |/ ____\ \        / /\   |  __ \     / / /  
 | |  | | |__) | |  | | |  __ \ \  /\  / /  \  | |__) |   / / /__ 
 | |  | |  _  /| |  | | | |_ | \ \/  \/ / /\ \ |  _  /   / / / _ \
 | |__| | | \ \| |__| | |__| |  \  /\  / ____ \| | \ \  / / /  __/
 |_____/|_|  \_\\____/ \_____|   \/  \/_/    \_\_|  \_\/_/_/ \___|
                                                                  
                                                                  
     The "Other" Commodies Exchange Game - In Applesoft BASIC
	       Copyright 2018 Jay Moore - A FOSS Game
	         http://gitlab.com/dewdude/drugwar2e
	         http://gitlab.com/dewdude/drugwar64

			 
Contents:

About
Instructions
Files
Updates
Support
```


About:

Drugwar//e is a remake of a TI-BASIC game that is itself based on
a 1984 IBM PC game written by John E. Dell. The game is based around
the buying and selling of drugs in "a major city". The user has to
contend with constantly changing prices, events designed to cause 
spikes and drops in prices, police trying to catch you, and a loan
shark you owe money. Your goal is to pay off your debt, earn the most
money you can, and make it out alive; all in a period of 30 days. You
are able to buy upgrades and even guns along the way.

Drugwar//e should run on any Apple ][ with Applesoft BASIC and 32k of
RAM. It has been tested with the AppleWin emulator.

I also ported it to Commodore Basic V2 based against a fixed 1.03
codebase which became Drugwar/64 1.01. You can find it on GitLab:
https://gitlab.com/dewdude/drugwar64

After the switch to GitLab, I decided to make a second project that was
just each revision submitted in order. BASIC from Alpha 1 up to 1.03 has
been committed; and VirtualBasic from Beta 1 up to 1.03 has also been
committed. It is only there for the curious. 
https://gitlab.com/dewdude/drugwar2ebetas

Instructions:

To play Drugwar//e, simply place the diskette in your Apple ][ and boot
it up! The game will automatically load.

Playing the game is rather simple. Various options on menus have a 
letter emphasised (L)ike (T)his. Merely enter the letter of the option 
you want. For yes or no questions, simply enter Y for yes; the computer
will accept anything else as N. To "back out" of a menu, simply enter
nothing at the prompt. Promps that require you to enter "0" to back
out indicate so in the game. Some interactions, like the police, you
cannot escape out of.
 

Drug prices change every day, and random events can cause them to be
really high or really low. You start out with a debt of $5000 that
you must pay the loan shark; interest on this loan is calculated
every day. The Loan Shark can only be visited in The Bronx. Also in
The Bronx is the bank, where you can deposit money in a savings
account to not only keep it safe; but collect daily interest.
 

Beware, both cops and muggers can be found on the subway. If you deal
too heavily, the police will come after you. Also found on the subway are 
chances to upgrade your abilities by purchasing a larger trechcoat for more inventory space as well as the ability to buy guns to fight the police. Be 
warned, every gun you buy consumes five inventory slots.

Buy low, sell high...just not on your own supply. Pay off your debt and
earn as much as possible. 

The game ends when you die or complete 30 days.


Files:

Your DRUGWAR2E.ZIP package contains the following file:

	DRUGWAR2E.DSK - Drugwar//e DOS 3.3 Disk Image
	drugwar2e.bas - Applesoft BASIC code
	drugwar2e.baz - Virtual Applesoft BASIC code
	README.md - This readme file
	LICENSE - FreeBSD 2-Clause Simplified LICENSE
	CHANGELOG - Detailed changelog

These files are also available indvidually on the project page.
	

	
Updates

28-MAY-2020
    Uploaded My TI-83 Debugged Code
    Started trying to disassemble original game
        Plan on doing more "authentic" version in maybe Python
        or Arduino. Might write 80-column A2 version.

5-NOV-2018: Version 1.05
	
	First all GitLab release - No more Sourceforge!
	Condensed Code Even More!!!
		First releases were over 700, last one was around 450. This one is under 300!!!!!
	Closed the big gaping cheat hole.
		Special "anti-cheat" system that lets you cheat a little
	Changed how cops react to inventory.
	Cops now shoot when you run.

29-OCT-2018: Version 1.04

	Added the missing goto after using the doctor. I don't know how I missed that.
	
	Changed how it displays the titlebar a bit.
	
	Modified health. Instead of going *to* 50, you start at 100. Police damage doubled
	to match new behavior. Minor but important to expanded version
	
	Cleaned up code..then realized I broke the game; fixed what broke, kept some cleaning.
	This means I may have missed broken interactions.
	
	The loader actually didn't do anything except look like it did stuff. I got rid of it
	(for now). Game now just auto-boots.
	

20-OCT-2018: Version 1.03

	I am dyslexic at times and my brain gets hungup on lowercase
	ps and qs...so any ludes you bought weren't getting counted.

	That's fixed. Thanks /u/FinalRewind

	I may have packaged 1.01's disk in 1.02's zip...negating the purpose
	of downloading it at all. Sorry. I didn't get much sleep.

20-OCT-2018: Version 1.02
	
	Caught missed bug related to when you smoke in the subway.
	Changed how it displays version number.

20-OCT-2018: Version 1.01
	
	This is the very first non-beta release. Version 1.0 was used for
	internal testing and not released. 
	
	Compared to Beta 9, this has all the text formatted properly as
	well as some "minor" code optimizations and clean-ups. Please see
	README.OLD for the complete Alpha/Beta update history.
	

	
Support
	
	If you encounter problems while running Drugwar//e, please make a
	note of the exact error (including line number) and file a ticket
	on the Sourceforge Project page; https://drugwar2e.sourceforge.io.
	
	You may also contact me by email: dewdude(at)gmail.com
	You may also find me on Twitter: _NQ4T (yes, underscore)
	

	
	
